'use strict';

var should = require('should');
var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var app = require('../../app');
var request = require('supertest');
var Document = require('./document.model');
var User = require('../user/user.model');

var user = new User({
  provider: 'local',
  name: 'Fake User',
  email: 'test@test.com',
  password: 'password'
});

var admin = new User({
  provider: 'local',
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'admin'
});

var newDocument = new Document({
  hash: 'lol'
});

var transaction = new bitcore.Transaction({
});

var seed = 'cook bachelor abstract goddess capital only response pact torch shift danger tower';
var mnemonic = new Mnemonic(seed);
var retrieved = new bitcore.HDPrivateKey('xpub6DQk1VraQZofc3r2VJwQ9xnbmHDnTxQzFHcry4dTfQ4R2L2SgRkouExH7nokNnpcmX5Hxtcp2PSzRwu16DsdH9pPrtTqTbX29URMjaUwzob')

describe('Document Model', function() {

  it('should generate a new pubkey', function(done){
    user.hdSeed = mnemonic;
    user.masterPrivKey = user.hdSeed.toHDPrivateKey('', 'livenet');
    newDocument.generatePubKey(user, function(err){
      if(err) { throw(err); }
      should.exist(newDocument.pubKey);
      done();
    });
  });

  it('should create a Multisig Address', function(done){
    user.hdSeed = new Mnemonic(Mnemonic.Words.ENGLISH);
    user.masterPrivKey = retrieved
    newDocument.generatePubKey(user, function(err){
      if(err) { throw(err); }
      newDocument.createMultisigAddress('03a488873ee9010f1e67f0e3bf1cf9994ea14cd3478b30e44bd21f83b1ef14446b', function(err){
        if(err) { throw(err); }
        should.exist(newDocument.addr);
        done();
      });
    });    
  });


  it('should grab a partially signed tx and finish creating it', function(done){
    transaction.sign(newDocument.privKey);
    var serialized = transaction.toObject();
    newDocument.createTransaction(serialized, newDocument.pubKey, [], function(err, rawTx){
      if(err) { throw(err); }
      should.exist(rawTx);
      done();
    });
  });
  

});
  