'use strict';

angular.module('diguSignApp')
  .controller('WalletCtrl', function ($scope, $http, Auth, User) {

  	$scope.balance = null;

  	$scope.user = Auth.getCurrentUser();

  	$scope.getBalance = function(){
  		User.balance($scope.user.currentAddress).then(function(data){
  			$scope.balance = (data.data / 100000000);
  		});
  	};

  });