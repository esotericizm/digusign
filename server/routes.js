/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var path = require('path');
var multipart = require('connect-multiparty');
var userController = require('./api/user/user.controller');
var _ = require('underscore');

module.exports = function(app) {

  app.use(multipart({
      uploadDir: './uploads'
  }));
  // Insert routes below
  app.use('/api/documents', require('./api/document'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  app.post('/upload', userController.imgUpload, function(req, res, next){
      var data = _.pick(req.body, 'type')
          , uploadPath = path.normalize('/uploads')
          , file = req.files.file;

      var imgName = file.path.toString();
      res.status(200).json(imgName.substring(8));
  });
  
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};
