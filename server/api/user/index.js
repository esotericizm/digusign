'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.post('/fund', auth.hasRole('admin'), controller.fund);
router.post('/image', auth.hasRole('admin'), controller.image);
router.post('/verify', auth.hasRole('admin'), controller.verify);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.post('/forgot', controller.forgotPassword);
router.post('/reset', controller.reset);

module.exports = router;
