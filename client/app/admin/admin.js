'use strict';

angular.module('diguSignApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin/accounts',
        templateUrl: 'app/admin/accounts/accounts.html',
        controller: 'AccountsCtrl'
      }).
      state('wallet', {
      	url: '/admin/wallet',
      	templateUrl: 'app/admin/wallet/wallet.html',
      	controller: 'WalletCtrl'
      });
  });