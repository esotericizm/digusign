'use strict';

var express = require('express');
var controller = require('./document.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id/check', auth.isAuthenticated(), controller.check);
router.get('/:id/find', controller.find);
router.post('/', auth.isAuthenticated(), controller.create);
module.exports = router;