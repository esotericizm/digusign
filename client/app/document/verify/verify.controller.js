'use strict';

angular.module('diguSignApp')
  .controller('VerifyCtrl', function ($scope, Auth, Document, $location, $http, store, notify) {

  	$scope.searchResults = null;

  		$scope.search = function(searchQuery){
  			$scope.searchResults = null;
        var newSearch = Document.find({id: searchQuery})
        newSearch.$promise.then(function(resp){
          console.log(resp)
          $scope.searchResults = resp.message;
        }, function(err){
          notify({ message: "Couldn't find that Document or txid", position: 'right', classes: "alert-danger danger-box"});
        });
  		}

  		$scope.translateTxid = function(txid){
  			$http.get('http://digiexplorer.info/api/tx/' + txid).
  				success(function(resp, status){
  					var vout = resp.vout[1].scriptPubKey.hex
  					vout = vout.substring(4);
					var hex = vout.toString();//force conversion
					var str = '';
					for (var i = 0; i < hex.length; i += 2)
						str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
					notify({ message: "Found an op_return transaction!: " + str, position: 'right', classes: "alert-primary danger-box"});
					$scope.search(str);
  				}).
  				error(function(err){
            $scope.search(txid);
  				});
  		}
  });
