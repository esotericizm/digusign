'use strict';

var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var User = require('../api/user/user.model');
var _ = require('lodash');
var validateJwt = expressJwt({ secret: config.secrets.session });

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
  return compose()
    // Validate jwt
    .use(function(req, res, next) {
      var authHeader = req.headers.authorization;

      if (authHeader) {
        var token = authHeader.split(' ');
        if (/^Bearer$/i.test(token[0]) && token[1]) {
          return jwt.verify(token[1], config.secrets.session, {}, function(err, decoded) {
            if (!err && decoded) {
              req.user = decoded;
            }

            next();
          });
        }
      }
      next();

    })
    // Attach user to request
    .use(function(req, res, next) {
      console.log(req.user)
      if(!req.user){
        User.findOne({userName: 'Guest'}, function(err, user){
          req.user = user;
          next();
        });
      } else {
        User.findById(req.user._id, function (err, user) {
          if (err) return next(err);
          if (!user) return res.status(401).send('Unauthorized');

          req.user = user;
          next();
        });
      }
    });
}

function isGuest() {
  return compose()
    /* Normalize Token */
    .use(normalizeToken)
    /* Check for token and decode if there */
    .use(function(req, res, next) {
      var authHeader = req.headers.authorization;

      if (authHeader) {
        var token = authHeader.split(' ');
        if (/^Bearer$/i.test(token[0]) && token[1]) {
          return jwt.verify(token[1], config.secrets.session, {}, function(err, decoded) {
            if (!err && decoded) {
              req.user = decoded;
            }

            next();
          });
        }
      }

      next();
    })
    /* Attach user if they have an id */
    .use(function(req, res, next) {
      var guest = { role: 'guest' };
      if (req.user && req.user._id) {
        return User.findById(req.user._id, function(err, user) {
          if (!err && user) {
            req.user = user
          }
          next();
        });
      }
      next();
    })
    /* Lastly attach guest user if no user has been found */
    .use(function(req, res, next) {
      if (!req.user || !req.user.role) {
        return User.findOne({userName: 'Guest'}, function(err, user) {
          if (!err && user) {
            req.user = user
          }
          next();
        });
      }
      next();
    });

}

function normalizeToken(req, res, next) {
  if (req.query && req.query.hasOwnProperty('access_token')) {
    req.headers.authorization = 'Bearer ' + req.query.access_token;
  }
  next();
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
  if (!roleRequired) throw new Error('Required role needs to be set');

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (config.userRoles.indexOf(req.user.role) >= config.userRoles.indexOf(roleRequired)) {
        next();
      }
      else {
        res.status(403).send('Forbidden');
      }
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id) {
  return jwt.sign({ _id: id }, config.secrets.session, { expiresInMinutes: 60*5 });
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
  if (!req.user) return res.status(404).json({ message: 'Something went wrong, please try again.'});
  var token = signToken(req.user._id, req.user.role);
  res.cookie('token', JSON.stringify(token));
  res.redirect('/');
}

exports.isAuthenticated = isAuthenticated;
exports.isGuest = isGuest;
exports.hasRole = hasRole;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;