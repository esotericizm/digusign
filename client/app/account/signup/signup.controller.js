'use strict';

angular.module('diguSignApp')
  .controller('SignupCtrl', function ($scope, Auth, $location, store, HDAddress) {

    $scope.user = {};
    $scope.errors = {};

    $scope.register = function(form) {
      $scope.submitted = true;

      if(form.$valid) {  
          var bitcore = require('bitcore-lib');
          var Mnemonic = require('bitcore-mnemonic');

          var hdSeed = new Mnemonic(Mnemonic.Words.ENGLISH);

          var obj = { seed: hdSeed.toString(), transactions: [] };

          var storage = store.get($scope.user.userName);
          if(!storage){
            store.set($scope.user.userName, JSON.stringify(obj));
          }
          store.set($scope.user.userName, JSON.stringify(obj));
          var keys = HDAddress.createNewKeyPair($scope.user.userName);

        Auth.createUser({
          firstName: $scope.user.firstName,
          lastName: $scope.user.lastName,
          userName: $scope.user.userName,
          email: $scope.user.email,
          password: $scope.user.password,
          initialPubKey: keys.pubKey.toString()
        })
        .then( function() {
          //store.set('user', user);
          $location.path('/');
        })
        .catch( function(err) {
          console.log(err)
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };

  });
