'use strict';

angular.module('diguSignApp')
  .controller('ForgotCtrl', function ($scope, User, $location, notify) {
    $scope.formData = {};
    $scope.errors = {};

    $scope.forgot = function(form) {
      $scope.submitted = true;

      User.API.forgotPass({email: $scope.formData.email}).$promise.then(function(body){
        console.log(body);
        notify({ message: body.message, position: 'right', classes: "alert-primary notify-box"});
      }, function(err){
        notify({ message: err.data.message, position: 'right', classes: "alert-danger notify-box"});
      })
      
    };

  });
