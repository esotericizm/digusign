'use strict';

angular.module('diguSignApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupCtrl'
      })
      .state('forgot', {
        url: '/account/forgot',
        templateUrl: 'app/account/forgot/forgot.html',
        controller: 'ForgotCtrl'
      })
      .state('reset', {
        url: '/account/reset/:id',
        templateUrl: 'app/account/reset/reset.html',
        controller: 'ResetCtrl'
      })
      .state('settings', {
        url: '/account/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        authenticate: true
      })
      .state('profile', {
        url: '/account/profile',
        templateUrl: 'app/account/profile/profile.html',
        controller: 'ProfileCtrl',
        authenticate: true
      })
      .state('history', {
        url: '/account/history',
        templateUrl: 'app/account/history/history.html',
        controller: 'HistoryCtrl',
        authenticate: true
      })
      .state('accountverify', {
        url: '/account/verify',
        templateUrl: 'app/account/accountverify/accountverify.html',
        controller: 'AccountVerifyCtrl',
        authenticate: true
      });
  });