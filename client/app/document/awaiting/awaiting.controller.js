'use strict';

angular.module('diguSignApp')
  	.controller('AwaitingCtrl', function ($scope, Auth, Document, User, $location, $stateParams, $http, $interval, $state, $rootScope) {
  		var currentId = $stateParams.id;

      $scope.Address = null;
      $scope.hash = null;

      (function(){
        var a = new Document.check({id: currentId});
        a.$promise.then(function(data){
          if(!data.message.txid){
            $scope.Address = data.message.addr;
            $scope.hash = data.message.hash;
            $scope.message = "Please send 2 DGB to " + $scope.Address + " in order to sign this digest";
            var uri = 'digibyte:'+ $scope.Address +'?amount=2';
            var qrcode = new QRCode('qrcode', {
              text: uri,
              width: 256,
              height: 256,
              correctLevel : QRCode.CorrectLevel.H
            });
            //$scope.$broadcast("loaded");         
          } else {
            $location.path(/document/ + data.message.hash);
          }
        }, function(err){
          console.log(a);
        });
      }());

    $scope.stop = $interval(checkBalance, 2000);

    var dereg = $rootScope.$on('$locationChangeSuccess', function() {
      $interval.cancel($scope.stop);
      dereg();
    });
    function checkBalance(){
      User.balance($scope.Address).then(function(data, status){
        if(data.data > 2){
          var checkDoc = Document.check({id: currentId});
          checkDoc.$promise.then(function(data){
            $location.path(/document/ + $scope.hash);
          })
        }
        return
      })
    }

    });