'use strict';

angular.module('diguSignApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {

    $scope.url = null;
    if(Auth.isLoggedIn()){
      console.log('lol')
      $scope.url = '/sign';
    } else {
      $scope.url = '/';
    }
    $scope.menu = [{
      'title': 'Sign',
      'link': '/sign'
    }, {
      'title': 'Verify',
      'link': '/verify'
    }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });