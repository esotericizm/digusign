'use strict';

angular.module('diguSignApp')
  .controller('MainCtrl', function ($scope, $http, store, Auth, $location) {
  	$scope.isLoggedIn = Auth.isLoggedIn;
    $scope.user = {};
    $scope.errors = {};

    $scope.login = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Logged in, redirect to home
          $location.path('/');
        })
        .catch( function(err) {
          $scope.errors.other = err.message;
        });
      }
    };

  $scope.myInterval = 3000;
  $scope.slides = [
    {
      image: 'assets/images/filing-cabinent.png',
      text: 'Easily certify any file type you want!'
    }/*,
    {
      image: 'http://www.abjsolutions.com.au/wp-content/uploads/2013/08/Certified-Bookkeepers.jpg',
      text: 'Verifiable proof that your file has not been altered since the time registerd on the blockchain'
    }*/
  ];

  });
