'use strict';

angular.module('diguSignApp')
  	.factory('HDAddress', function HDAddress($location, $rootScope, $http, User, Auth, store, $q) {
  		var bitcore = require('bitcore-lib');
	  	var Mnemonic = require('bitcore-mnemonic');
	    var currentUser = Auth.getCurrentUser();

	    return {

	    	createNewKeyPair: function(user){
	    		var seedIndex = 0;
	    		var seed = null;
	    		if(user){
	    			seed = JSON.parse(store.get(user));
	    			seedIndex = 0;
	    		} else {
	    			if(!currentUser.storedDocuments){
	    				seedIndex = 0;
	    			} else {
	    				seedIndex  = currentUser.storedDocuments.length
	    			}
	    			console.log(seedIndex)
	    			seed = JSON.parse(store.get(currentUser.userName));
	    		}
		    	seed.seed = new Mnemonic(seed.seed);
				var xpriv = seed.seed.toHDPrivateKey();
				var derived = xpriv.derive("m/44'/0'/0'/0/" + (seedIndex) );
				var derivedHdPublicKey = derived.hdPublicKey;
				var privKey = derived.privateKey;
				var pubKey = derivedHdPublicKey.publicKey;;
				var newKeyPair = { pubKey: pubKey, privKey: privKey};
				return newKeyPair;
			},
			nextPubKey: function(user){
				var seedIndex = 0;
				var seed = null;
	    		if(user){
	    			seed = JSON.parse(store.get(user));
	    			seedIndex = 0;
	    		} else {
	    			if(!seedIndex.storedDocuments){
	    				seedIndex = 0;
	    			} else {
	    				seedIndex = currentUser.storedDocuments.length;
	    			}
	    			seed = JSON.parse(store.get(currentUser.userName));
	    		}
				seed.seed = new Mnemonic(seed.seed.toString());
				var xpriv = seed.seed.toHDPrivateKey();
				var derived = xpriv.derive("m/44'/0'/0'/0/" + ( seedIndex + 1));
				var derivedHdPublicKey = derived.hdPublicKey;
				var pubKey = derivedHdPublicKey.publicKey;;
				return pubKey;
			},
			getUtxos: function(address){
				var defer = $q.defer();
				$http.get('https://digiexplorer.info/api/addr/' + address + '/utxo').success(function(data){
					defer.resolve(data);
				});
				return defer.promise;
			},
			createMultisigAddress: function(pubkey1, pubkey2){
				var newAddr = null;
				console.log(pubkey1, pubkey2)
				if(pubkey2){
					newAddr = new bitcore.Address([pubkey1, pubkey2], 2);
					return newAddr.toString();
				} else {
					newAddr = new bitcore.Address([pubkey1], 1);
					return newAddr.toString();
				}
			}

	    };
	});