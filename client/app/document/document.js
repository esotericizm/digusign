'use strict';

angular.module('diguSignApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sign', {
        url: '/sign',
        templateUrl: 'app/document/sign/sign.html',
        controller: 'SignCtrl'
      }).
      state('document', {
      	url: '/document/:id',
      	templateUrl: 'app/document/docs/document.html',
      	controller: 'DocumentCtrl'
      }).
      state('awaiting', {
        url: '/document/awaiting/:id',
        templateUrl: 'app/document/awaiting/awaiting.html',
        controller: 'AwaitingCtrl'
      }).
      state('verify', {
        url: '/verify',
        templateUrl: 'app/document/verify/verify.html',
        controller: 'VerifyCtrl'
      });
  });