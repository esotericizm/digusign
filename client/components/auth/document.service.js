'use strict';

angular.module('diguSignApp')
  .factory('Document', function ($resource, $stateParams) {
    return $resource('/api/documents/:id/:controller', {
      id: '@_id'
    },
    {
      check: {
        method: 'GET',
        params: {
          id: '@_id',
          controller: 'check',
        }
      },
      create: {
        method: 'POST',
        params: {
          id: '@_id'
        }
      },
      find: {
        method: 'GET',
        params: {
          id: '@_id',
          controller: 'find'
        }
      }
	  });
  });
