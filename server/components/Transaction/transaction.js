'use strict';

var request = require('request');
var bitcore = require('bitcore-lib');
var async = require('neo-async');
var Mnemonic = require('bitcore-mnemonic');
var User = require('../../api/user/user.model');

function Transaction(){
	this.user = this.user;
	this.privKey;
	this.fromAddress;
	this.toAddress;
	this.changeAddress;
	this.pubKey = this.pubKey;
	this.userPubKey = this.userPubKey;
	this.amount = this.amount || 100000000;
	this.utxo = this.utxo || null;
	this.txObj = this.txObj;
	this.hashData = this.hashData;
	this.transaction = this.transaction;
	this.txid = this.txid;
}

Transaction.prototype.generateSeed = function generateSeed() {
    var self = this;
    self.hdSeed = new Mnemonic(Mnemonic.Words.ENGLISH);
    return self.hdSeed;
  },

Transaction.prototype.createMultisigAddress = function createMultisigAddress(pubkey1, pubkey2){
	var newAddr = null;
	if(pubkey2){
		newAddr = new bitcore.Address([pubkey1, pubkey2], 2);
		return newAddr.toString();
	} else {
		newAddr = new bitcore.Address([pubkey1], 1);
		return newAddr.toString();
	}
}

Transaction.prototype.createNewKeyPair = function createNewKeyPair(seed, index, cb){
	var self = this;
	seed = new Mnemonic(seed);
	var xpriv = seed.toHDPrivateKey();
	var derived = xpriv.derive("m/44'/0'/0'/0/" + index);
	var derivedHdPublicKey = derived.hdPublicKey;
	var privKey = derived.privateKey;
	var pubKey = derivedHdPublicKey.publicKey;
	var addr = self.createMultisigAddress(pubKey);
	var newKeyPair = { addr: addr, pubKey: pubKey, privKey: privKey};

	return cb(null, newKeyPair);
}

Transaction.prototype.getUtxo = function getUtxo(addr, cb){
	var self = this;
	var url = 'https://digiexplorer.info/api/addr/' + addr + '/utxo';
	request(url, function(err, res, body){
		if(err){ return cb(new Error("Can't query digiexplorer, is it down?")); }
		var json;
		try {
			json= JSON.parse(body);
		} catch(e){
			return cb(e);
		}
		if(json.length === 0 || json.amount < 2 || !json){
			return cb(new Error("Please transfer atleast 2 DGB to the payment address"));
		}
		self.utxo = json;
		return cb(null, json);
	});
}

Transaction.prototype.createFundingTx = function createFundingTx(toAddr, changeAddr, pubKey, privKey, fundingAmount, cb){
	var self = this;
	fundingAmount = fundingAmount * 100000000;
	var newBitcoreTransaction = new bitcore.Transaction()
		.from(self.utxo, [pubKey], 1)
		.to(toAddr, fundingAmount)
		.change(changeAddr)
		.fee(100000000)
		.sign([privKey])

	self.transaction = newBitcoreTransaction.serialize();

	return cb(null);
}

Transaction.prototype.createGuestTx = function createGuestTx(cb){
	var self = this;
	async.waterfall([
		function(callback){
			User.findOne({userName: 'Admin'}, function(err, admin){
				if(err || !admin) { return callback(err); }
				callback(null, admin);
			});
		},
		function(admin, callback){
			var newPriv = new bitcore.PrivateKey();
			var newAddr = newPriv.toAddress();
			var newBitcoreTransaction = new bitcore.Transaction()
				.from(self.utxo)
				.to(newAddr, 100000000)
				.change(admin.currentAddress)
				.addData(self.hash)
				.fee(100000000)
				.sign(self.privKey)

			self.transaction = newBitcoreTransaction.serialize();
			callback(null);	
		}
	], function(err){
		if(err) { return cb(err); }
		return cb(null);
	});
}

Transaction.prototype.sendDocumentTx = function sendDocumentTx(ownPubKey, userPubKey, changeAddress, hash, privKey, privKey2, cb){
	var self = this;
	console.log(ownPubKey, userPubKey, changeAddress, hash, privKey, privKey2);
	var newPriv = new bitcore.PrivateKey();
	var newAddr = newPriv.toAddress();
	var newBitcoreTransaction = new bitcore.Transaction()
		.from(self.utxo, [ownPubKey, userPubKey], 2)
		.to(newAddr, 100000000)
		.change(changeAddress)
		.addData(hash)
		.fee(100000000)
		.sign(privKey)
	newBitcoreTransaction.sign(privKey2)

	self.transaction = newBitcoreTransaction.serialize();

	return cb(null);
}

Transaction.prototype.broadcastTx = function broadcastTx(cb){
	var self = this;
	request.post('http://digiexplorer.info/api/tx/send', { form: {rawtx: self.transaction}}, function(err, res, body){
		console.log(err, res, body)
	    if(err){ return cb(new Error("couldn't broadcast the transaction\n    Reason: " + err)); }
	    var json;
	    try {
	        json = JSON.parse(body);
	    } catch(e){
	        return cb(e);
	    }
		self.txid = json.txid;
		return cb(null, json.txid);
	});
}

module.exports = Transaction;