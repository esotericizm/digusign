'use strict';

var User = require('./user.model');
var Transaction = require('../../components/transaction/transaction');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('neo-async');
var path = require('path');
var fs = require('fs');
var crypto = require('crypto');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport('smtps://DiguSign%40gmail.com:DiguSign2016!@smtp.gmail.com');


function checkResetPassword(req, res){
  User.findOne({ resetPasswordToken: String(req.body.token), resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      return res.status(400).json({success: false, message: 'Password reset token is invalid or has expired.'});
    }
    return res.status(200).json({success: true})
  });  
}

function resetPassword(req, res){
  async.waterfall([
    function(callback) {
      User.findOne({ resetPasswordToken: String(req.body.token), resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        console.log(err, user, req.params.id)
        if (!user) {
          return callback(new Error("Password reset token is invalid or has expired."));
        }

        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          callback(null, user);
        });
      });
    },
    function(user, callback) {
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      transporter.sendMail(mailOptions, function(err) {
        callback(null);
      });
    }
  ], function(err) {
    if(err) { return res.status(400).json({success: false, message: err.toString() }); }
    return res.status(200).json({success: true, message: "Password succesfully changed...Please login"})
  });  
}

var validationError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.status(500).send(err);
    res.status(200).json(users);
  });
};

// Handles the image upload.
exports.imgUpload = function(req, res, next) {
  var file = req.files.file
  var imgName = file.path.toString();
  User.findOne({userName: req.body.username}, function (err, user) {
    user.identificationFile = imgName.substring(8);
    user.awaitingVerification = true;
    user.save(function(err){
      if(err) return res.status(500).send(err);
      next();
    });
  });
};


/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var token = null;

  async.waterfall([
    function(callback){

      var newUser = new User(req.body);
      newUser.provider = 'local';
      var hdSeed = new Transaction().generateSeed();
      newUser.hdSeed = hdSeed.toString();
      var masterPrivKey = hdSeed.toHDPrivateKey();
      var derived = masterPrivKey.derive("m/44'/0'/0'/0/0");
      newUser.currentPrivKey = derived.privateKey;
      var derivedHdPublicKey = derived.hdPublicKey;
      newUser.currentPubKey = derivedHdPublicKey.publicKey;

      if(newUser.userName === 'Admin'){
        newUser.currentAddress = new Transaction().createMultisigAddress(newUser.currentPubKey);
        newUser.role = 'admin';
      } else {
        newUser.currentAddress = new Transaction().createMultisigAddress(newUser.currentPubKey, newUser.initialPubKey);
      }
      callback(null, newUser);
    }, function(newUser, callback){
      newUser.save(function(err, user) {
        if(err) { return callback(err); }
        token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
        callback(null, user);
      });
    }
  ], function(err, user){
      if(err) { return res.status(400).json({success: false, message: err}); }
      return res.json({ token: token});
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).send('Unauthorized');
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.status(500).send(err);
    return res.status(204).send('No Content');
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.status(200).send('OK');
      });
    } else {
      res.status(403).send('Forbidden');
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).send('Unauthorized');
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};

/**
 * View a users ID documentation
 */
exports.image = function(req, res, next) {
  var userId = req.body.userName;
  User.findOne({userName: userId}, function(err, user){
    var img = fs.readFileSync(path.resolve('./uploads/' + user.identificationFile));
    res.writeHead(200, {'Content-Type': 'image/jpeg' });
    res.end(img, 'binary');
  });
}

/**
 * Verify an account initiated by an admin
 */
exports.verify = function(req, res, next) {
  var userId = req.body.userName;
  User.findOne({userName: userId}, function(err, user){
    console.log(user)
    user.verifiedIdentification = true;
    user.awaitingVerification = false;
    user.save(function(err){
      if(err){ return res.status(400).json({success: false, message: err}); }
      return res.status(200).json({success: true});
    });
  });
}

/**
 * Fund an account initiated by an admin
 */
exports.fund = function(req, res, next) {
  var userId = req.body.userName;
  var fundingAmount = req.body.amount;
  console.log(userId)
  async.waterfall([
    function(callback){
      User.findOne({userName: 'Admin'}, function(err, admin){
        if(err){ return res.status(400).json({success: false, message: err}); }
        callback(null, admin);
      });
    }, function(admin, callback){
      User.findOne({userName: userId}, function(err, user){
        if(err){ return res.status(400).json({success: false, message: err}); }
        user.balance += fundingAmount;
        callback(null, admin, user);
      });
    }, function(admin, user, callback){
      console.log(fundingAmount)
      var newTransaction = new Transaction();
      newTransaction.getUtxo(admin.currentAddress, function(err){
        if(err) { return callback(err); }
        newTransaction.createNewKeyPair(admin.hdSeed, admin.oldAddresses.length + 1, function(err, keyPair){
          if(err) { return callback(err); }
          newTransaction.createFundingTx(user.currentAddress, keyPair.addr.toString(), admin.currentPubKey , admin.currentPrivKey, fundingAmount, function(err){
            if(err) { return callback(err); }
            newTransaction.broadcastTx(function(err){
              if(err) { return callback(err); }
              admin.oldAddresses.push({privkey: admin.currentPrivKey, pubkey: admin.currentPubKey, addr: admin.currentAddress});
              admin.currentAddress = keyPair.addr.toString();
              admin.currentPrivKey = keyPair.privKey.toString();
              admin.currentPubKey  = keyPair.pubKey.toString();
              callback(null, admin, user, newTransaction.txid);
            });
          });
        });
      });
    }, function(admin, user, tx, callback){
      user.save(function(err){
        if(err){ return callback(err); }
        callback(null, admin, tx);
      });
    }, function(admin, tx, callback){
      admin.save(function(err){
        if(err){ return callback(err); }
        callback(null, tx);
      });
    }
  ], function(err, tx){
    if(err) { return res.status(400).json({success: false, message: err.toString()}) }
    return res.status(200).json({success: true, message: tx})
  });

};


exports.forgotPassword = function(req, res, next){
  async.waterfall([
    function(callback) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        callback(null, token);
      });
    },
    function(token, callback) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          return callback(new Error("No account with that email address exists"))
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          callback(null, token, user);
        });
      });
    },
    function(token, user, callback) {
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@digusign.com',
        subject: 'DiguSign Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/account/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      transporter.sendMail(mailOptions, function(err) {
        callback(null);
      });
    }
  ], function(err) {
    if (err) { return res.status(400).json({success: false, message: err}); }
    return res.status(200).json({success: true, message: 'An e-mail has been sent to ' + req.body.email + ' with further instructions.'});
  });
};

exports.reset = function(req, res, next){
  if(!req.body.password){
    checkResetPassword(req, res);
  } else {
    resetPassword(req, res);
  }
};