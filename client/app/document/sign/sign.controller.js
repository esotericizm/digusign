'use strict';

angular.module('diguSignApp')
  .controller('SignCtrl', function ($scope, Auth, Document, $location, $http, store, notify, HDAddress, $timeout) {
  	var bitcore = require('bitcore-lib');

  	$scope.max = 100;
   	$scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress
    });


	$scope.docTypes = [
		{ name: "None", code: null },
		{ name: "Purchase Order  (POR)", code: "POR" }, 
		{ name: "CMR Document (CMR)", code: "CMR" },
		{ name: "Commercial Invoice (CIV)", code: "CIV" },
		{ name: "Airway Bill (AWB)", code: "AWB" },
		{ name: "Packing List (PLT)", code: "PLT" },
		{ name: "Bill of Lading (BOL)", code: "BOL" },
		{ name: "Letter of Credit (LOC)", code: "LOC" },
		{ name: "Multimodal Bill of Lading (FBL)", code: "FBL" },
		{ name: "Certificate of Origin (COO)", code: "COO" },
		{ name: "Inspection Certificate (ICT)", code: "ICT" },
		{ name: "Insurance Document (IDC)", code: "IDC" },
		{ name: "Tax Document (TDC)", code: "TDC" },
		{ name: "Real Estate Document (RDC)", code: "RDC" },
		{ name: "Estate Document (EDC)", code: "EDC" },
		{ name: "Bank Account Document (BDC)", code: "BDC" },
		{ name: "Identification Document (IDT)", code: "IDT" },
		{ name: "Contract Document (CDC)", code: "CDC" },
		{ name: "Legal Document (LGD)", code: "LGD" },
		{ name: "Non Disclosure Agreement (NDA)", code: "NDA" },
		{ name: "Employment Agreement (EAT)", code: "EAT" },
		{ name: "Memorandum of Understanding (MOU)", code: "MOU" },
		{ name: "Terms of Use (TOU)", code: "TOU" },
		{ name: "Privacy Policy (PPY)", code: "PPY" },
		{ name: "Business Plan (BPL)", code: "BPL" },
		{ name: "Operating Agreement (OAT)", code: "OAT" }
	]

	var crypto_callback = function(p) {
		var w = ((p*100).toFixed(0));
	}
	
	var crypto_finish = function(hash) {
		$scope.hash = hash.toString();
		$scope.$apply();
		notify({ message: "File hashed successfully: " + hash, position: 'right', classes: "alert-primary notify-box"});
	}

	$scope.selected = $scope.docTypes[0];

    $scope.$watch('files', function () {
    	if($scope.files){
        	$scope.upload($scope.files);
        }
    });
    $scope.$watch('file', function () {
        if ($scope.file != null) {
            $scope.files = [$scope.file]; 
        }
    });
    $scope.log = '';

    $scope.upload = function (file) {
	    var reader = new FileReader();
	    notify({ message: "File being uploaded please wait:", position: 'right', classes: "alert-primary notify-box"});
		reader.onload = function(e) {
			var data = e.target.result;
			//var myWorker = Webworker.create(CryptoJS.SHA256(data, crypto_callback, crypto_finish), {async: true });
			$timeout(function(){
				CryptoJS.SHA256(data, crypto_callback, crypto_finish);
			}, 1000)
			
		};
		reader.onprogress = function(evt) {
		    if (evt.lengthComputable) {
		    	$scope.$broadcast("fileProgress", (((evt.loaded / evt.total)*100).toFixed(2)));
		    }
		}
		reader.readAsBinaryString(file);
    };

    $scope.documentSubmit = function(){
    	var currentUser = Auth.getCurrentUser();
    	console.log(currentUser.currentPrivKey)

    	if(Auth.isLoggedIn()){
	    		var keyPair = HDAddress.createNewKeyPair();
	    		var nextPubKey = HDAddress.nextPubKey();
			    var pubKey = keyPair.pubKey;

			    var newDocument = new Document.create({id: $scope.hash, privKey: keyPair.privKey.toString(), pubKey: keyPair.pubKey.toString(), nextPubKey: nextPubKey})
			    newDocument.$promise.then(function(data){
			    	console.log(data);
			    	$location.path('/document/' + $scope.hash);
			    }, function(err){
			    	console.log(err);
			    });
    	} else {
    		var sendResouce = new Document.create({id: $scope.hash, user: currentUser});
    		sendResouce.$promise.then(function(data){
    			notify({ message: "Upload successfull please wait to be redirected to the payment page: " + JSON.stringify(data.message.hash), position: 'right', classes: "alert-primary danger-box"});
    			$location.path('/document/awaiting/' + $scope.hash);
    		}, function(err){
    			console.log(err);
    		});

	    }
    }
  });
