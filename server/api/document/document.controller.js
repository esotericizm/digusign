/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /documents              ->  index
 * POST    /documents              ->  create
 * GET     /documents/:id          ->  show
 * PUT     /documents/:id          ->  update
 * DELETE  /documents/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var User = require('../user/user.model');
var Document = require('./document.model');
var Transaction = require('../../components/transaction/transaction');
var async = require('neo-async');

function createGuestDocument(req, res){
  var userId = req.user.userName;
  var hash = req.body.id || null;
  var docType = req.body.docType || null;
  var newDoc =  new Document();
  var timestamp = Math.floor(Date.now() / 1000);

  var hashString = null;
  if (docType){
    hashString = docType + " " + hash;
  }
  hashString = hash;

  async.waterfall([
    function(callback){
      newDoc.hash = hash;
      newDoc.type = docType;
      newDoc.timeRegistered = timestamp;
      newDoc.generatedAddrPair(function(err){
        if(err){ return callback(err); }
        newDoc.save(function(err){
          if(err) { return callback(err); }
          callback(null, newDoc);
        });
      });
    }, function(newDoc, callback){
      User.findOne({userName: 'Guest'}, function(err, user){
        if(user.storedDocuments.length > 99){
          user.storedDocuments.pop();
        }
        user.storedDocuments.push(newDoc._id);
        user.save(function(err){
          if(err) { return callback(err); }
          callback(null, newDoc);
        });
      });
    }
  ], function(err, newDoc){
    if(err) { return res.status(400).json({success: false, message: err}); }
    return res.status(200).json({success:true, message: { address: newDoc.addr, hash: newDoc.hash}});
  });
}

function createMemberDocument(req, res){
  var userId = req.user.userName;
  var hash = req.body.id || null;
  var docType = req.body.docType || null;
  var txObj = req.body.txObj || null;
  var userPubKey = req.body.pubKey || null;
  var newDoc =  new Document();
  var timestamp = Math.floor(Date.now() / 1000);

  var hashString = null;
  if (docType){
    hashString = docType + " " + hash;
  }
  hashString = hash;

  async.waterfall([
    function(callback){
      User.findOne({userName: userId}, function(err, user){
        if(err){ return callback(err); }
        callback(null, user);
      });
    },
    function(user, callback){   
      var newTransaction = new Transaction();
      var userPubKey = null;
      newTransaction.createNewKeyPair(user.hdSeed, user.storedDocuments.length, function(err, keyPair){
        if(err) { return res.status(400).json({success:false, message: "Couldn't generate Keypair"}); }
        newTransaction.createNewKeyPair(user.hdSeed, user.storedDocuments.length, function(err, pubKey){
          if(err) { return res.status(400).json({success:false, message: "Couldn't generate Keypair"}); }
          var changeAddr = new Transaction().createMultisigAddress(pubKey.pubKey, req.params.nextPubKey);
          newTransaction.getUtxo(user.currentAddress, function(err){
            newTransaction.sendDocumentTx(user.currentPubKey, req.body.pubKey, changeAddr.toString(), hashString, user.currentPrivKey, req.body.privKey, function(err){
              if(err) { return res.status(400).json({success: false, message: "Couldn't create the transaction"}); }
              newTransaction.broadcastTx(function(err, tx){
                if(err) { return res.status(400).json({success:false, message: "Couldn't broadcast the Tx"}); }
                user.balance -= 2;
                user.currentPubKey = pubKey.pubKey;
                user.currentPrivKey = pubKey.privKey;
                user.currentAddress = changeAddr;
                callback(null, user, tx)
              });
            });
          });
        });
      });
    },
    function(user, tx, callback){
      newDoc.hash = hash;
      newDoc.txid = tx;
      newDoc.docType = docType;
      newDoc.isSent = true;
      newDoc.timeRegistered = timestamp;
      newDoc.timeBroadcast = timestamp;
      newDoc.save(function(err){
        if(err) { return res.status(400).json({success: false, message: "Failed to save Document"}); }
        callback(null, tx, user);
      });
    },
    function(tx, user, callback){
      user.storedDocuments.push(hash);
      user.save(function(err){
        if(err) { return res.status(400).json({success: false, message: "Couldn't save user"}); }
        callback(null, tx)
      });
    }
  ], function(err, tx){
    if(err) { return res.json({success: false, message: err}) }
    return res.status(200).json({success: true, message: {txid: tx, hash: hash}});
  });

}

// Get list of documents
exports.index = function(req, res) {
  Document.find(function (err, documents) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(documents);
  });
};

// Find a document (search result)
exports.find = function(req, res) {
  Document.find({hash: req.params.id}, function(err, doc){
    console.log(doc)
    if(err || !doc[0]){
      return res.status(400).json({success:false, message: "Couldn't find Document"});
    }
    return res.status(200).json({success: true, message: {hash: doc[0].hash, txid: doc[0].txid, timeRegistered: doc[0].timeRegistered, timeBroadcast: doc[0].timeBroadcast}});
  });
};

// Check to see if guest doc has been paid for if so broadcast it
exports.check = function(req, res) {
  var timestamp = Math.floor(Date.now() / 1000);

  async.waterfall([
    function(callback){
      Document.findOne({hash: req.params.id}, function(err, doc){
        if(err){
          return callback(new Error("Couldn't find document"));
        } else if (doc.isSent === true) {
          return res.status(200).json({success: true, message: {txid: doc.txid, hash: req.body.id}});
        }
        callback(null, doc);
      });
    }, function(doc, callback){
      var newTransaction = new Transaction()
      newTransaction.privKey = doc.privKey;
      newTransaction.fromAddress = doc.addr;
      newTransaction.amount = 200000000;
      newTransaction.hash = req.params.id;
      newTransaction.getUtxo(doc.addr, function(err){
        if(err) { return res.status(200).json({success: true, message: { addr: doc.addr, hash: doc.hash}}) }
        newTransaction.createGuestTx(function(err){
          if(err) { return callback(err); }
          newTransaction.broadcastTx(function(err, tx){
            if(err) { return callback(err); }
            callback(null, doc, tx)
          });
        });
      });
    }, function(doc, tx, callback){
      doc.txid = tx;
      doc.isSent = true;
      doc.timeBroadcast = timestamp;
      doc.save(function(err){
        if(err) { return callback(err); }
        callback(null, doc);
      })
    }
  ], function(err, doc){
    if(err) { res.status(400).json({success: false, message: err}); }
    res.status(200).json({success: true, message: {txid: doc.txid, hash: doc.hash}});
  });
};

// Creates a new document in the DB. If user is guest we send them the document digest and payment address
exports.create = function(req, res) {

  if(req.user.userName === 'Guest'){
    createGuestDocument(req, res);
  } else {
    createMemberDocument(req, res);
  }

};

// Updates an existing document in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Document.findById(req.params.id, function (err, document) {
    if (err) { return handleError(res, err); }
    if(!document) { return res.status(404).send('Not Found'); }
    var updated = _.merge(document, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(document);
    });
  });
};

// Deletes a document from the DB.
exports.destroy = function(req, res) {
  Document.findById(req.params.id, function (err, document) {
    if(err) { return handleError(res, err); }
    if(!document) { return res.status(404).send('Not Found'); }
    document.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
