'use strict';

angular.module('diguSignApp')
  .factory('User', function ($resource, $http) {
    var root = {};
    root.API = $resource('/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      viewImage: {
        method: 'POST',
        params: {
          id: 'image'
        },
        responseType: "blob",
        transformResponse: function (data) {
            var pdf = new Blob([data], {
                type: 'image/jpeg'
            });
          return  {
            response: pdf
          };
        }
      },
      verify: {
        method: 'POST',
        params: {
          id: 'verify'
        }
      },
      fundAccount: {
        method: 'POST',
        params: {
          id: 'fund'
        }
      },
      forgotPass: {
        method: 'POST',
        params: {
          id: 'forgot'
        }
      },
      resetPassword: {
        method: 'POST',
        params: {
          id: 'reset'
        }
      }
	  });
    root.balance = function(address){
      return $http.get('http://digiexplorer.info/api/addr/' + address  + '/balance').
        success(function(data, stats){
          return data;
        }).
        error(function(err){
          return err;
        });
    }
        
    return root;
  });
