'use strict';

angular.module('diguSignApp')
  .controller('ProfileCtrl', function ($scope, User, Auth, store, HDAddress) {

  	$scope.user = Auth.getCurrentUser();

  	$scope.seed = store.get($scope.user.userName);
  	$scope.masterPrivKey = store.get('masterPrivKey');
  	var keyPair = HDAddress.createNewKeyPair();

  	console.log($scope.user, keyPair.pubKey.toString(), keyPair.privKey.toString())

  });