'use strict';

var should = require('should');
var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var app = require('../../app');
var User = require('./user.model');

var user = new User({
  provider: 'local',
  name: 'Fake User',
  email: 'test@test.com',
  password: 'password'
});

var admin = new User({
  provider: 'local',
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'admin'
});

var seed = 'cook bachelor abstract goddess capital only response pact torch shift danger tower';
var mnemonic = new Mnemonic(seed);
var retrieved = new bitcore.HDPrivateKey('xpub6DQk1VraQZofc3r2VJwQ9xnbmHDnTxQzFHcry4dTfQ4R2L2SgRkouExH7nokNnpcmX5Hxtcp2PSzRwu16DsdH9pPrtTqTbX29URMjaUwzob')

describe('User Model', function() {
  /*before(function(done) {
    // Clear users before testing
    User.remove().exec().then(function() {
      done();
    });
  });*/

  afterEach(function(done) {
    User.remove().exec().then(function() {
      done();
    });
  });

  it('should begin with no users', function(done) {
    User.find({}, function(err, users) {
      users.should.have.length(0);
      done();
    });
  });

  it('should fail when saving a duplicate user', function(done) {
    user.save(function() {
      var userDup = new User(user);
      userDup.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  it('should fail when saving without an email', function(done) {
    user.email = '';
    user.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it("should authenticate user if password is valid", function() {
    return user.authenticate('password').should.be.true;
  });

  it("should not authenticate user if password is invalid", function() {
    return user.authenticate('blah').should.not.be.true;
  });

  it("should generate a new HD seed", function(done){
    user.generateSeed(function(err){
      should.exist(user.hdSeed);
      should.exist(user.masterPrivKey);
      done();
    });
  });

  it('should derive a new keypair', function(done){
    user.hdSeed = new Mnemonic(Mnemonic.Words.ENGLISH);
    user.masterPrivKey = user.hdSeed.toHDPrivateKey('', 'livenet');
    user.getNewAddrPair(function(err){
      should.exist(user.currentAddress)
      done();
    });
  });

});
