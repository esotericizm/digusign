'use strict';

angular.module('diguSignApp')
  .controller('AccountVerifyCtrl', function ($scope, User, Auth, Upload, store, notify) {

  	$scope.user = Auth.getCurrentUser();

  	$scope.show = false;


  	if(!$scope.user.identificationFile){
  		$scope.show = true;
  	}

    $scope.upload = function (file) {
        Upload.upload({
            url: 'upload',
            data: {file: file, 'username': $scope.user.userName}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.show = false;
            notify({ message: "File uploaded.. You will be verified as soon as possible", position: 'right', classes: "alert-primary notify-box"});
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };


  });