/**
 * The data-layer for a message wall
 * @module models
 */

 'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');


/**
 * @constructor User
 */
var UserSchema = new Schema({
  firstName: { type: String, index: true },
  lastName: { type: String, index: true },
  userName: { type: String, index: true, unqiue: true },
  email: { type: String, lowercase: true, unqiue: true },
  role: {
    type: String,
    default: 'user'
  },
  hashedPassword: String,
  provider: String,
  salt: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  verifiedEmail: { type: Boolean, default: false},
  initialPubKey: {type: String },
  currentAddress: { type: String, default: null },
  currentPubKey: { type: String, default: null },
  currentPrivKey: { type: String, default: null },
  oldAddresses: { type: Array },
  balance: { type: Number, default: 0 },
  identificationFile: { type: String, default: null },
  verifiedIdentification: { type: Boolean, default: false },
  storedDocuments: { type: Array },
  hdSeed: { type: Object, default: null },
  pendingAccounts: { type: Array},
});

/**
 * Virtuals
 */
UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() {
    return this._password;
  });

// Public profile information
UserSchema
  .virtual('profile')
  .get(function() {
    return {
      'name': this.firstName + this.lastName,
      'role': this.role
    };
  });

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(function() {
    return {
      '_id': this._id,
      'role': this.role
    };
  });

/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function(email) {
    return email.length;
  }, 'Email cannot be blank');

// Validate empty password
UserSchema
  .path('hashedPassword')
  .validate(function(hashedPassword) {
    return hashedPassword.length;
  }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function(value, respond) {
    var self = this;
    this.constructor.findOne({email: value}, function(err, user) {
      if(err) throw err;
      if(user) {
        if(self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
}, 'The specified email address is already in use.');

UserSchema
  .path('userName')
  .validate(function(value, respond) {
    var self = this;
    this.constructor.findOne({userName: value}, function(err, user) {
      if(err) throw err;
      if(user) {
        if(self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
}, 'The specified username is already in use.');

var validatePresenceOf = function(value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', function(next) {
    if (!this.isNew) return next();
    if (!validatePresenceOf(this.hashedPassword))
      next(new Error('Invalid password'));
    else
      next();
  });

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function() {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Encrypt password
   *
   * function encryptPassword
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function(password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  },

  /**
  * Takes a mnemonic and generates a keypair from it 
  *
  * @function getNewAddrPair
  * @memberof module:models~User
  * @this module:models~User
  * @param {String} seed - Mnemonic
  * @param {Object} callback - KeyPair or null.
  */

  getNewAddrPair: function(seed, cb){
    var self = this;
    if (!self.hdSeed){
      self.generateSeed(function(err){ if(err){ return cb(err); }} );
    } else if(self.currentAddress){
      self.oldAddressPairs.push({privKey: self.currentPrivKey, pubKey: self.currentPubKey, addr: self.currentAddress});
    }
    self.currentPrivKey = self.masterPrivKey.privateKey;
    self.currentPubKey = self.masterPrivKey.publicKey;
    self.currentAddress  = bitcore.Address.fromPublicKey(self.currentPubKey);
    return cb(null, {privKey: self.currentPrivKey, pubKey: self.currentPubKey, Address: self.currentAddress});
  },

  fundAccount: function(accountName, cb){
    var self = this;
    self.constructor.findOne({userName: accountName}, function(err, user) {
      user.getNewAddrPair(function(err){

      });
    });
  }
};

module.exports = mongoose.model('User', UserSchema);
