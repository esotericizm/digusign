'use strict';

angular.module('diguSignApp')
  .controller('DocumentCtrl', function ($scope, Auth, Document, $location, $stateParams, $http, $timeout, $state, store) {
    var currentId = $stateParams.id;

      var newDocument = new Document.find({id: currentId});
      newDocument.$promise.then(function(resp){
          $scope.hash = resp.message.hash;
          $scope.txid = resp.message.txid;
          $scope.timeRegistered = resp.message.timeRegistered;
          $scope.timeBroadcast = resp.message.timeBroadcast;
          console.log(resp)
      }, function(err){
        console.log(err);
      });


  });
