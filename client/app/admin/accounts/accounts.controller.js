'use strict';

angular.module('diguSignApp')
  .controller('AccountsCtrl', function ($scope, $http, Auth, User, notify) {

    $scope.isFirstOpen = true;
    $scope.isOpen = false;

    // Use the User $resource to fetch all users
    $scope.users = User.API.query();
    $scope.userObj = $scope.users

    $scope.downloadImage = function(userName){

      var newImage = User.API.viewImage({userName: userName.userName});
      newImage.$promise.then(function(data){
        var url = URL.createObjectURL(data.response);
        window.open(url);
      }, function(err){
        console.log(err);
      });
    };

    $scope.delete = function(user) {
      User.remove({ id: user._id });
      angular.forEach($scope.users, function(u, i) {
        if (u === user) {
          $scope.users.splice(i, 1);
        }
      });
    };

    $scope.verify = function(user) {
      var newVerify = new User.API.verify({userName: user.userName, amount: 0});
      newVerify.$promise.then(function(data){
        user.verifiedIdentification = true;
        user.awaitingVerification = false;
        notify({ message: "User Successfully verified", position: 'right', classes: "alert-primary"});
      }, function(err){
        notify({ message: "Couldn't verifiy user, please refresh and try again", position: 'right', classes: 'alert-danger danger-box'});
      });
    };

    $scope.fundAccount = function(userName, amount){
      var newFund = new User.API.fundAccount({userName: userName.userName, amount: amount});
      newFund.$promise.then(function(data){
        userName.balance += amount;
        notify({ message: "User successfully funded", position: 'right', classes: "alert-primary"});
      }, function(err){
        console.log(err);
        notify({ message: "Couldn't fund this User...Try again later", position: 'right', classes: "alert-danger danger-box"});
      });
    };

    $scope.isVerified = function(bool){
      if(bool){
        $scope.userObj = [];
        angular.forEach($scope.users, function(user){
          if(user.verifiedIdentification === true){
            $scope.userObj.push(user);
          }
        });
      } else {
        $scope.userObj = $scope.users;
      }
    };

    $scope.awaitingVerification = function(bool){
      if(bool){
        $scope.userObj = [];
        angular.forEach($scope.users, function(user){
          if(user.verifiedIdentification === false && user.identificationFile){
            $scope.userObj.push(user);
          }
        });
      } else {
        $scope.userObj = $scope.users;
      }
    };
  });
