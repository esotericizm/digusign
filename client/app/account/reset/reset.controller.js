'use strict';

angular.module('diguSignApp')
  .controller('ResetCtrl', function ($scope, User, $location, $stateParams, $timeout, notify) {
    var currentId = $stateParams.id;
    $scope.errors = {};
    

    User.API.resetPassword({token: currentId}).$promise.then(function(body){
      notify({ message: 'Token valid...Please enter a new Password', position: 'right', classes: "alert-primary notify-box"});
      return
    }, function(err){
      notify({ message: err.data.message, position: 'right', classes: "alert-danger notify-box"});
      $location.path('/');
    });

    $scope.reset = function(){
      console.log($scope.errors)
      $scope.submitted = true;

      User.API.resetPassword({token: currentId, password: $scope.user.password}).$promise.then(function(resp){
        console.log(resp)
        notify({ message: resp.message, position: 'right', classes: "alert-primary notify-box"});
        $timeout(function(){
          $location.path('/login');
        }, 3000)
      }, function(err){
        notify({ message: err.data.message, position: 'right', classes: "alert-danger notify-box"});
      })
    }

  });
