'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var bitcore = require('bitcore-lib');
var request = require('request');
var async = require('neo-async');
var User = require('../user/user.model');


/**
* @constructor Document
*/
var DocumentSchema = new Schema({
  hash: { type: String, index: true, default: null, unique: true },
  docType: { type: String },
  addr: { type: String, index: true },
  pubKey: { type: String, index: true },
  privKey: { type: String },
  isSent: { type: Boolean, default: false },
  txid: { type: String },
  timeRegistered: { type: Number },
  timeBroadcast: { type: Number }
});

/**
 * Methods
 */

DocumentSchema.methods = {

	/**
	* Generates a new standard address pair for guest accounts
	*
	* @function generatedAddrPair
	* @memberof module:models~Document
	* @this module:models~Document
	* @param {Object} callback - KeyPair or null.
	*/	
	generatedAddrPair: function(cb){
		var self = this;
		var newPrivKey = new bitcore.PrivateKey();
		var newAddr = newPrivKey.toAddress();
		self.privKey = newPrivKey.toString();
		self.addr = newAddr.toString();
		return cb(null, {privKey: self.privKey, Address: self.addr});
	},

	/**
	* Creates a new Document for fully funded users
	*
	* @function newDocument
	* @memberof module:models~Document
	* @this module:models~Document
	* @param {Object} user - The user object.
	* @param {String} userPubKey - the users pubkey generated client side.
	* @param {Object} callback - error or null.
	*/	
	newDocument: function(user, userPubKey, cb){
		var self = this;
		async.waterfall([
			function(callback){
				self.generatePubKey(user, function(err){
					if(err) { return callback(err) };
					callback(null);
				});
			}, function(callback){
				self.createMultisigAddress(userPubKey, function(err){
					if(err) { return callback(err) };
					callback(null);
				});
			}, function(callback){
				self.getFundingUtxo(function(err, utxo){
					if(err) { return callback(err) };
					callback(null, utxo);
				});
			}, function(utxo, callback){
				self.fundMultisigAddress(utxo, function(err, rawtx){
					if(err) { return callback(err) };
					callback(null, rawtx);
				});
			}, function(rawtx, callback){
				self.broadcastTransaction(rawtx, function(err){
					if(err) { return callback(err) };
					callback(null, rawtx);
				});
			},
		], function(err){
			if(err) { return cb(err) }
			return cb(null);
		});
	},

	payDocument: function(txObj, userPubKey, cb){
		var self = this;
		async.waterfall([
			function(callback){
				self.checkForUtxo(function(err, utxo){
					if(err){
						return callback(err)
					}
					callback(null, utxo);
				});
			}, function(utxo, callback){
				self.createTransaction(txObj, userPubKey, utxo, function(err, rawtx){
					if(err){
						return callback(err);
					}
					callback(null, rawtx);
				});
			}, function(rawtx, callback){
				self.broadcastTransaction(rawtx, function(err){
					if(err) {
						return callback(err);
					}
					callback(null);
				});
			}
		], function(err){
			if(err){ return cb(err); }
			return cb(null);
		});
	}

};

module.exports = mongoose.model('Document', DocumentSchema);